// Copyright (c) 2022 NodeOps
//
// Distributed under the GNU GPLv3 software license, see the accompanying
// file LICENSE or visit <https://www.gnu.org/licenses/gpl-3.0.en.html>

#include <cstdlib>

#include "hello/hello.hpp"

int main() {
  hello(std::cout, std::string("Hello, Friend"));
  return EXIT_SUCCESS;
}
