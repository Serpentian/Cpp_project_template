// Copyright (c) 2022 NodeOps
//
// Distributed under the GNU GPLv3 software license, see the accompanying
// file LICENSE or visit <https://www.gnu.org/licenses/gpl-3.0.en.html>

#ifndef CORE_HELLO_HPP
#define CORE_HELLO_HPP

#include <iostream>
#include <string>

// some king of cool interface
void hello(std::ostream& out, const std::string& str);

#endif
